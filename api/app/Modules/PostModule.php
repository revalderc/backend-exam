<?php

namespace App\Modules;

use App\Post;
use App\Comment;

class PostModule
{
    public function all()
    {
        return Post::paginate(10);
    }

    public function create($data = [])
    {
        $post = Post::create($data);

        if (!$post) {
            return null;
        }

        return $post;
    }

    public function getOneBySlug($slug)
    {
        $post = Post::where('slug', $slug)->first();

        if (!$post) {
            return null;
        }

        return $post;
    }

    public function updateBySlug($slug, $data = [])
    {
        $post = Post::where('slug', $slug)->first();

        if (!$post) {
            return null;
        }

        $post->fill($data);
        $post->save();

        return $post;
    }

    public function deleteBySlug($slug)
    {
        $post = Post::where('slug', $slug)->first();

        if (!$post) {
            return null;
        }

        return $post->delete();
    }
}
