<?php

namespace App\Modules;

use App\Comment;

class CommentModule
{
    public function getByPost($postId)
    {
        return Comment::where('commentable_id', $postId)
            ->where('commentable_type', 'App\\\Post')
            ->get();
    }

    public function create($data = [])
    {
        $comment = Comment::create($data);

        if (!$comment) {
            return null;
        }

        return $comment;
    }

    public function update($id, $data = [])
    {
        $comment = Comment::find($id);

        if (!$comment) {
            return null;
        }

        $comment->fill($data);
        $comment->save();

        return $comment;
    }

    public function delete($id)
    {
        $comment = Comment::find($id);

        if (!$comment) {
            return null;
        }

        return $comment->delete();
    }
}
