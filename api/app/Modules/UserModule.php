<?php

namespace App\Modules;

use App\User;

class UserModule
{

    public function create($data = [])
    {
        $user = User::create($data);

        if (!$user) {
            return null;
        }

        return $user;
    }
}
