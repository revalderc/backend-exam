<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:3',
        ]);

        $data['password'] = \Hash::make($data['password']);

        if (!$user = (new \App\Modules\UserModule)->create($data)) {
            return response()->json([
                'message' => 'Failed to create account. Please try again.'
            ], 409);
        }

        return $user; // HAS to do it like this to satisfy the front-end request.

        // return response()->json([
        //     'data' => $user
        // ], 201);
    }

    public function login(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'email' => ['These credentials do not match our records.']
                ]
            ], 422);
        }

        return response()->json([
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    public function logout()
    {
        return auth()->logout();
    }
}
