<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Modules\PostModule;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $postModule;

    public function __construct(PostModule $post)
    {
        $this->postModule = $post;
    }

    public function index()
    {
        $posts = $this->postModule->all();

        return response()->json([
            'data' => $posts->items()
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $request->user();

        $data = $request->validate([
            'title' => 'required',
            'content' => 'required',
            'image' => 'required',
        ]);

        $data['slug'] = str_slug($data['title']);
        $data['user_id'] = $user->id;

        if (!$post = $this->postModule->create($data)) {
            return response()->json([
                'message' => 'Failed to create post. Please try again.'
            ], 409);
        }

        return response()->json([
            'data' => $post
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        if (!$post = $this->postModule->getOneBySlug($slug)) {
            return response()->json([
                'message' => 'No query results for model [App\\\Post]'
            ], 404);
        }

        return response()->json([
            'data' => $post
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $data = $request->validate([
            'title' => 'required'
        ]);

        if (!$post = $this->postModule->updateBySlug($slug, $data)) {
            return response()->json([
                'message' => 'No query results for model [App\\\Post]'
            ], 404);
        }

        return response()->json([
            'data' => $post
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        if (!$this->postModule->deleteBySlug($slug)) {
            return response()->json([
                'message' => 'Failed to delete post.'
            ], 409);
        }

        return response()->json([
            'status' => 'record deleted successfully'
        ], 200);
    }
}
