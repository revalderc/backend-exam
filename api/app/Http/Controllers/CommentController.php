<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\PostModule;
use App\Modules\CommentModule;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $postModule;
    protected $commentModule;

    public function __construct(PostModule $post, CommentModule $comment)
    {
        $this->postModule = $post;
        $this->commentModule = $comment;
    }

    public function index($postSlug)
    {
        $post = $this->postModule->getOneBySlug($postSlug);

        if (!$post) {
            return response()->json([
                'message' => 'No query results for model [App\\\Post]'
            ], 404);
        }

        $comments = $this->commentModule->getByPost($post->id);

        return response()->json([
            'data' => $comments
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $postSlug)
    {
        $user = request()->user();

        $data = $request->validate([
            'body' => 'required'
        ]);

        if (!$post = $this->postModule->getOneBySlug($postSlug)) {
            return response()->json([
                'message' => 'No query results for model [App\\\Post]'
            ], 404);
        }

        if (!$comment = $this->commentModule->create([
            'creator_id' => $user->id,
            'commentable_id' => $post->id,
            'commentable_type' => 'App\\\Post',
            'body' => $data['body']
        ])) {
            return response()->json([
                'message' => 'Failed to create comment.'
            ], 409);
        }

        return response()->json([
            'data' => $comment
        ], 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $postSlug, $id)
    {
        $data = $request->validate([
            'body' => 'required'
        ]);

        if (!$comment = $this->commentModule->update($id, [
            'body' => $data['body']
        ])) {
            return response()->json([
                'message' => 'Failed to update comment.'
            ], 409);
        }

        return response()->json([
            'data' => $comment
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($postSlug, $id)
    {
        if (!$this->commentModule->delete($id)) {
            return response()->json([
                'message' => 'Failed to delete comment.'
            ], 409);
        }

        return response()->json([
            'status' => 'record deleted successfully'
        ], 200);
    }
}
