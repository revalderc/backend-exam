## Setup

### 1. Fill the ff. `.env` variables:
- DB_DATABASE=
- DB_USERNAME=
- DB_PASSWORD=

### 2. Run the ff. comands:
- `php artisan migrate`
- `php artisan db:seed` for a test user

### 3. Login the test user account
- Email/User ID: `testuser@gmail.com` 
- Password: `123456`

Thank you!