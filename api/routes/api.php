<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth'], function () {
    Route::post('/posts', 'PostController@store');
    Route::patch('/posts/{slug}', 'PostController@update');
    Route::delete('/posts/{slug}', 'PostController@destroy');

    Route::post('/posts/{slug}/comments', 'CommentController@store');
    Route::patch('/posts/{slug}/comments/{id}', 'CommentController@update');
    Route::delete('/posts/{slug}/comments/{id}', 'CommentController@destroy');

    Route::post('/logout', 'AuthController@logout');
});

Route::get('/posts', 'PostController@index');
Route::get('/posts/{postSlug}', 'PostController@show');
Route::get('/posts/{postSlug}/comments', 'CommentController@index');

Route::post('/login', 'AuthController@login');
Route::post('/register', 'AuthController@register');
